/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Dao.AccountCustomerDAO;
import Dao.OrderStatusDAO;
import Dao.PaymentMethodDAO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author doanq
 */
public class Order {
    private String order_id,customer_id,order_date,order_total_price,order_status,payment_method;

    public Order() {
    }

    public Order(String order_id, String customer_id, String order_date, String order_total_price, String order_status, String payment_method) {
        this.order_id = order_id;
        this.customer_id = customer_id;
        this.order_date = order_date;
        this.order_total_price = order_total_price;
        this.order_status = order_status;
        this.payment_method = payment_method;
    }

    

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    

    public String getOrder_date() {
        String currentFormat = "yyyy-MM-dd";
        String newFormat = "dd/MM/yyyy";
        try {
            SimpleDateFormat sdfCurrent = new SimpleDateFormat(currentFormat);
            SimpleDateFormat sdfNew = new SimpleDateFormat(newFormat);
            Date date = sdfCurrent.parse(order_date);
            String newDateString = sdfNew.format(date);

            return newDateString;
        } catch (ParseException e) {
            e.printStackTrace();
        }
            return null;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    

    public String getOrder_total_price() {
        return order_total_price;
    }

    public void setOrder_total_price(String order_total_price) {
        this.order_total_price = order_total_price;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    @Override
    public String toString() {
        return "Order{" + "order_id=" + order_id + ", customer_id=" + customer_id + ", order_date=" + order_date + ", order_total_price=" + order_total_price + ", order_status=" + order_status + ", payment_method=" + payment_method + '}';
    }

    

    
    public String getCustomer_name() {
        AccountCustomerDAO acd = new AccountCustomerDAO();
        return acd.getAccountCustomerById(customer_id).getCustomer_name();
    }
    
}
