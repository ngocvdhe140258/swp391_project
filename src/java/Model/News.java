/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author doanq
 */
public class News {
    private String news_id,news_title,news_content,news_created_date,news_modified_date,news_created_by,news_modified_by;

    public News() {
    }

    public News(String news_id, String news_title, String news_content, String news_created_date, String news_modified_date, String news_created_by, String news_modified_by) {
        this.news_id = news_id;
        this.news_title = news_title;
        this.news_content = news_content;
        this.news_created_date = news_created_date;
        this.news_modified_date = news_modified_date;
        this.news_created_by = news_created_by;
        this.news_modified_by = news_modified_by;
    }

    public String getNews_id() {
        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_content() {
        return news_content;
    }

    public void setNews_content(String news_content) {
        this.news_content = news_content;
    }

    public String getNews_created_date() {
        return news_created_date;
    }

    public void setNews_created_date(String news_created_date) {
        this.news_created_date = news_created_date;
    }

    public String getNews_modified_date() {
        return news_modified_date;
    }

    public void setNews_modified_date(String news_modified_date) {
        this.news_modified_date = news_modified_date;
    }

    public String getNews_created_by() {
        return news_created_by;
    }

    public void setNews_created_by(String news_created_by) {
        this.news_created_by = news_created_by;
    }

    public String getNews_modified_by() {
        return news_modified_by;
    }

    public void setNews_modified_by(String news_modified_by) {
        this.news_modified_by = news_modified_by;
    }

    @Override
    public String toString() {
        return "News{" + "news_id=" + news_id + ", news_title=" + news_title + ", news_content=" + news_content + ", news_created_date=" + news_created_date + ", news_modified_date=" + news_modified_date + ", news_created_by=" + news_created_by + ", news_modified_by=" + news_modified_by + '}';
    }
    
}
